from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn import metrics
from dataPreprocessing import DataPreprocessing
from sklearn.externals import joblib
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

class ModelBuild:

    def decisionTree(data):
        print(data.head())
        data = DataPreprocessing.normalizeData(data)

        X = data[["ageGroup", "gender", "location", "ethnicity", "bloodPressure", "cholesterol", "allergic", "asthama",
                  "bodyTemp", "bodyPain"]].values

        y= data['medicine']
        # dividing train and test set
        X_trainset, X_testset, y_trainset, y_testset = train_test_split(X, y, test_size=0.3, random_state=3)

        # creating and training model
        model = DecisionTreeClassifier(criterion="entropy", max_depth=4)
        model.fit(X_trainset, y_trainset)
        # plt.imshow(img, interpolation='nearest')
        model.fit(X_trainset, y_trainset)

        prediction = model.predict(X_testset)


        # evaluating model
        print("DecisionTrees's Accuracy: ", metrics.accuracy_score(y_testset, prediction))
        print(prediction)
        print(y_testset[0:5])
        return model

    def logisticRegression(data):
        data = DataPreprocessing.normalizeData(data)

        X = data[["ageGroup", "gender", "location", "ethnicity", "bloodPressure", "cholesterol", "allergic", "asthama",
                  "bodyTemp", "bodyPain"]].values

        y = data['medicine']
        # dividing train and test set
        X_trainset, X_testset, y_trainset, y_testset = train_test_split(X, y, test_size=0.3, random_state=3)

        logreg = LogisticRegression()

        # fit the model with data
        logreg.fit(X_trainset, y_trainset)

        #Predict the test data
        y_pred = logreg.predict(X_testset)
        print("DecisionTrees's Accuracy: ", metrics.accuracy_score(y_testset, y_pred))
        cnf_matrix = metrics.confusion_matrix(y_testset, y_pred)
        print(cnf_matrix)


    def naiveBayes(data):
        data = DataPreprocessing.normalizeData(data)

        X = data[["ageGroup", "gender", "location", "ethnicity", "bloodPressure", "cholesterol", "allergic", "asthama",
                  "bodyTemp", "bodyPain"]].values

        y = data['medicine']
        # dividing train and test set
        X_trainset, X_testset, y_trainset, y_testset = train_test_split(X, y, test_size=0.3, random_state=3)

        gnb = GaussianNB()

        # fit the model with data
        gnb.fit(X_trainset, y_trainset)

        #Save Model
        # joblib.dump(gnb, 'model.pkl')


        #Predict the test data
        y_pred = gnb.predict(X_testset)
        print("Naive Bayes's Accuracy: ", metrics.accuracy_score(y_testset, y_pred))
        cnf_matrix = metrics.confusion_matrix(y_testset, y_pred)
        names = np.unique(y_pred)
        sns.heatmap(cnf_matrix, square=True, annot=True, fmt='d', cbar=False,
                    xticklabels=names, yticklabels=names)
        plt.xlabel('Truth')
        plt.ylabel('Predicted')
        # plt.show()

    def randomForestClassifier(data):
        data = DataPreprocessing.normalizeData(data)

        X = data[["ageGroup", "gender", "location", "ethnicity", "bloodPressure", "cholesterol", "allergic", "asthama",
                  "bodyTemp", "bodyPain"]].values

        y = data['medicine']
        # dividing train and test set
        X_trainset, X_testset, y_trainset, y_testset = train_test_split(X, y, test_size=0.3, random_state=3)

        rfc = RandomForestClassifier(n_estimators=100)

        # fit the model with data
        rfc.fit(X_trainset, y_trainset)

        #Predict the test data
        y_pred = rfc.predict(X_testset)
        print("Random Forest's Accuracy: ", metrics.accuracy_score(y_testset, y_pred))
        cnf_matrix = metrics.confusion_matrix(y_testset, y_pred)
        names = np.unique(y_pred)
        sns.heatmap(cnf_matrix, square=True, annot=True, fmt='d', cbar=False,
                    xticklabels=names, yticklabels=names)
        plt.xlabel('Truth')
        plt.ylabel('Predicted')
        # plt.show()















