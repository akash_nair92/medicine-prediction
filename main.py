from flask import Flask, request
import pandas as pd
from sklearn.externals import joblib






data = pd.read_csv("./Data/data.csv")




######## Post API Example
from flask import Flask, jsonify
app = Flask(__name__)
@app.route('/predict', methods=['POST'])
def predict():
     json_ = request.json
     query_df = pd.DataFrame(json_)
     pred_data = pd.get_dummies(query_df)

     # Load the saved model
     lr = joblib.load('model.pkl')

     # Load the Label Encoder for Medicine
     le_medicine = joblib.load("le_medicine.pkl")

     prediction = lr.predict(pred_data)
     return jsonify({'prediction': str(le_medicine.inverse_transform(prediction)[0])})

if __name__ == '__main__':
    app.run(port=5000, debug=True)




