import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import preprocessing
from sklearn.externals import joblib


class DataPreprocessing:

    def normalizeData(data):

        ## Divide Age column into bins
        data['ageGroup'] = pd.cut(x=data['age'], bins=[14, 23, 31, 39, 48, 57, 75], labels=[20, 30, 40, 50, 60, 70])
        data['ageGroup'] = data['ageGroup'].astype('int')

        print(data.head())

        # ## Label Encoder
        DataPreprocessing.labelEncodeCol(data, "gender")
        DataPreprocessing.labelEncodeCol(data, "location")
        DataPreprocessing.labelEncodeCol(data, "ethnicity")
        DataPreprocessing.labelEncodeCol(data, "bloodPressure")
        DataPreprocessing.labelEncodeCol(data, "cholesterol")
        DataPreprocessing.labelEncodeCol(data, "allergic")
        DataPreprocessing.labelEncodeCol(data, "asthama")
        DataPreprocessing.labelEncodeCol(data,"bodyPain")
        DataPreprocessing.labelEncodeCol(data, "medicine")

        ####### Univariate Distrtibution
        # sns.distplot(data["ageGroup"]);
        # plt.show()


        # ############## Heat Map ###########
        plt.figure(figsize=(5, 5))
        data=data[["medicine","ageGroup", "gender", "location", "ethnicity", "bloodPressure", "cholesterol", "allergic", "asthama", "bodyTemp", "bodyPain"]]
        cor = data.corr()
        sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
        plt.show()

        return data



    def labelEncodeCol(data,col_name):

        le_col_var_name = 'le_'+col_name
        le_col_var_name= preprocessing.LabelEncoder()
        le_col_var_name.fit(list(data[col_name].unique()))
        data[col_name] = le_col_var_name.transform(data[col_name])
        print(data.head())
        joblib.dump(le_col_var_name, "le_"+col_name+'.pkl')


