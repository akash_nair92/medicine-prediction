# Medicine Predicition

The project involves predicting the medicine for a particilar person from demographic data like age, gender, etc. and basic medical checks like blood pressure, cholestrol, etc. 

## Data:

The data contains demographic data and basic common medical checks of patients.
"Medicine" is our target variable. It is a categorical column containing 5 medicines: MedA, MedB, MedC, MedD and MedE.


age|gender|location|ethnicity|bloodPressure|cholestrol|allergic|asthama|bodyTemp|medicine
---|---|---|---|---|---|---|---|---|---|
23|Female|India|Brown|High|High|Yes|Yes|97|MedD
47|Male|China|Asian|Low|High|No|No|98|MedC

## Data Preprocessing:

The data contains categorical as well as continuous variables. We need to convert the categorical columns into continuous values for are model building.

* age column is first converted into multile bins and then lables (20,30,40,50,60) is given.
* Other categorical variables are treated using Label-Encoder which converts the categorical values to numbers.
* The label encoders for each column is fit and saved as a pickle object, so that we can use it for inverse transformation on the test data or the 
data used to predict the outcome.

## Heat Map

![HeatMap](./HeatMap.JPG)

As the data used is dummy data with just 200 data points, independent variables are not that strongly correlated with the dependent variable (medicine).
Hence, all the variables were used for building the model.


## Model Building:

Models were build using multiple classifier algorithms like: Decision Tree, Random Forest, Naive Bayes and Logistics regression.
Among all these algorithms, Naive Bayes gave better results. Below is the acuracy results of all the algorithms used.

Algorithm|Accuracy
---|---
Decision Tree| 55%
Random Forest | 50%
Naive Bayes | 61 %
Logistic Regression| 45%

The Naive Bayes model is saved in pickle format and used for the prediction.


## How to run:

The app is published using Flash API on 5000 port.

* First run main.py \
        python3 main.py
* Using Postman we can sent the request:

![PostmanResponse](./PostmanResponse.JPG)




 
 